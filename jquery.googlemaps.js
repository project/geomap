/* 
 * This file is based on :  
 * jQuery googleMap Copyright Dylan Verheul <dylan@dyve.net>
 * Licensed like jQuery, see http://docs.jquery.com/License
 * 
 * Some initial modifications made by Peter Brownell for Code Positive and
 * School of Everything. 
 *
 * We have made some modifications to the original file, but not that many. 
 * There is a good amount of work to do to tie this into Dupal and make
 * it configurable and themeable. 
 * 
 * For now, unless you want to patch the source, there is not all that
 * much to can do to change things. 
 * 
 * Ideas and plans welcome. 
 */

$(document).ready(function()
{
  $("#map").googleMap(null,null,13 , { markers: $(".geo"),controls: ["GSmallZoomControl"]});
});

$.googleMap = {
  maps: {},
  marker: function(m) {
    if (!m) {
      return null;
    } 
    else if (m.lat == null && m.lng == null) {
      return $.googleMap.marker($.googleMap.readFromGeo(m));
    } 
    else {
      // Instantiate icon with default image & shadow
      var tinyIcon = new GIcon(G_DEFAULT_ICON);      
      var dg = new Boolean();
      dg = false;
      if (m.draggable) {
        if (m.draggable == "true"){
          dg = true;
        }
      }
 ///======================HACKKKKKK
      tinyIcon.iconSize = new GSize(32, 32);
      tinyIcon.shadowSize = new GSize(32, 38);
      tinyIcon.image = 'http://schoolofeverything.com/sites/default/themes/everything2/images/star_pointer.png';
      tinyIcon.shadow = 'http://schoolofeverything.com/sites/default/themes/everything2/images/star_shadow.png';

      //use custom icon if set
      if (m.gicon == 'null') {
          tinyIcon.image = null;
      }
      else if (m.gicon) {
        tinyIcon.image = m.gicon;
      } 
      //use custom shadow if set
      if (m.gshadow == 'null'){
          tinyIcon.shadow = null;
      }
      else if (m.gshadow){
       tinyIcon.shadow = m.gshadow;
      }
      //if icon is set but no shadow is set, display no shadow
      else if (m.icon && !m.gshadow){
        tinyIcon.shadow = "";
      }
     
      tinyIcon.iconAnchor = new GPoint(15, 15); // set to centre
      tinyIcon.infoWindowAnchor = new GPoint(5, 1);
      
      markerOptions = { icon:tinyIcon, draggable:dg };
      
      var marker = new GMarker(new GLatLng(m.lat, m.lng), markerOptions );
      if (m.bind) {
          GEvent.addListener(marker, "click", function() {
          marker.openInfoWindowHtml('<div class="geomap-infowindow">'+$(m.bind).html()+'</div>');
        });
      } 
      else if (m.txt) {
        GEvent.addListener(marker, "click", function() {
        marker.openInfoWindowHtml(m.txt);
        });
      }
      if (m.link) {
        GEvent.addListener(marker, "dblclick", function() {
          window.location=m.link; 
        });
      }
      // set plain title for clusters
      marker.titleplain=m.titleplain;
      if (dg) {
        
          GEvent.addListener(marker, "dragend", function() {
            //confirm whether to register a new location
            if (confirm('Pointer icon has been relocated.\nWould you like to update the location information?')){
              var newpoint = marker.getLatLng();
              //alert ("services/location/geonames/latlng/"+m.nid+"/"+m.cckfield+"/"+m.cckfieldindex+"/"+newpoint.lat()+"/"+newpoint.lng()+"/background");
              var domain_name = document.domain;
              //for testing
              if (domain_name == "localhost"){
                domain_name = "localhost/map";
              }
              $.ajax({
                type: "GET",
                url: "http://"+domain_name+"/services/location/geonames/latlng/"+m.nid+"/"+m.cckfield+"/"+m.cckfieldindex+"/"+newpoint.lat()+"/"+newpoint.lng()+"/background",
                error: function(msg){
                  alert( "Sorry, relocation failed");
                },
                success: function(msg){
                  alert("Relocation successful");
                }
              });
          }
          //cancel
          else {
            alert("Relocation cancelled by user:\n\nPointer icon was moved, but location information was not updated.\nReloading the page will place the icon back to its original location.");
          }
        });
        
        
      }       
      return marker;
    }
  },
  readFromGeo: function(elem) {
    var latElem = $(".latitude", elem)[0];
    var lngElem = $(".longitude", elem)[0];
    var title = ''; 
    var titleplain = '';
    var link = '';
    if (latElem && lngElem) {
      titleplain = $(elem).attr("title");
      if ( $(elem).attr("title") ) {
        if ( $(elem).attr("nid") > 0 ) {
          link = '/node/'+$(elem).attr("nid");
          title = '<h4 class="maptitle"><a href="'+link+'">'+$(elem).attr("title")+'</a></h4>'; 
          if ( $(elem).attr("info") ) { title += $(elem).attr("info"); }
        } 
        else {
          title = $(elem).attr("title");
        }
      }
      
      return { lat:parseFloat($(latElem).attr("title")), lng:parseFloat($(lngElem).attr("title")), 
               txt:title, link: link, bind: $(elem).attr('bind'), 
               gicon: $(elem).attr('gicon'), gshadow: $(elem).attr('gshadow'),  draggable: $(elem).attr('draggable'),
               cckfield: $(elem).attr('cckfield'), cckfieldindex: $(elem).attr('cckfieldindex'), nid: $(elem).attr("nid"),
               titleplain: $(elem).attr("title")
               }
    } 
    else {
      return null;
    }
  },
  mapNum: 1
};

 
$.fn.googleMap = function(lat, lng, zoom, options) {

  // If we aren't supported, we're done
  if (!window.GBrowserIsCompatible || !GBrowserIsCompatible()) return this;
  // Default values make for easy debugging
  if (lat == null) lat = 51.52177;
  if (lng == null) lng = -0.20101;
  if (!zoom) zoom = 1;
  // Sanitize options
  if (!options || typeof options != 'object') options = {};
  options.mapOptions = options.mapOptions || {};
  options.markers = options.markers || [];
  options.controls = options.controls || {};

  // Map all our elements
  return this.each(function() {
    // Make sure we have a valid id
    if (!this.id) this.id = "gMap" + $.googleMap.mapNum++;
    // find our markers
    var marker = null;
    var markers = new Array();
    var bounds = null;
    for (var i = 0; i < options.markers.length; i++) {
      if (marker = $.googleMap.marker(options.markers[i])) {
        markers[i] = marker;
        if (!bounds) { 
          var bounds = new GLatLngBounds(marker.getPoint());
        } 
        else {
          // extend bounds with marker.getPoint();
          bounds.extend(marker.getPoint());
        }
      }
    }
    // we only want a map if we have markers
    if (markers.length) {
      $(this).css('display','block');
      // Create a map and a shortcut to it at the same time
      var map = $.googleMap.maps[this.id] = new GMap2(this, options.mapOptions); 
      var clusterer = new Clusterer(map);
      clusterer.SetMaxVisibleMarkers(20); 
      clusterer.SetMaxLinesPerInfoBox(5);

      ClustererIcon = new GIcon();
      ClustererIcon.image = '/sites/default/themes/everything2/images/star_pointer_multiple.png';
      ClustererIcon.shadow = '/sites/default/themes/everything2/images/star_shadow_multiple.png';
      ClustererIcon.iconSize = new GSize( 74, 74 );
      ClustererIcon.shadowSize = new GSize( 74, 79 );
      ClustererIcon.iconAnchor = new GPoint( 37, 37 );
      ClustererIcon.infoWindowAnchor = new GPoint( 50, 4 );
      ClustererIcon.infoShadowAnchor = new GPoint( 50, 0 );

      clusterer.SetIcon(ClustererIcon);
      
      // Center and zoom the map
      map.setCenter(new GLatLng(lat, lng), zoom);
      // Add controls to our map
      for (var i = 0; i < options.controls.length; i++) {
        var c = options.controls[i];
        eval("map.addControl(new " + c + "());");
      }
      for (var i = 0; i < markers.length; i++) {
        //map.addOverlay(markers[i]);
        clusterer.AddMarker( markers[i], markers[i].titleplain );
      }
      // time to zoom the map
      var distance = 0.015;
      if ( markers.length == 1 ) {
        // if we only have one marker, we move out a bit more
        distance = 0.5;
      }
      // Moving the map to show our markers
      // We have to set the centre to get the bounds properly
      
      map.setCenter(bounds.getCenter(), map.getBoundsZoomLevel(bounds));
      var southWest=bounds.getSouthWest();
      var northEast=bounds.getNorthEast();
      bounds.extend(new GLatLng(southWest.lat() - distance, southWest.lng() - distance ));
      bounds.extend(new GLatLng(northEast.lat() + distance, northEast.lng() + distance ));                 
      map.setCenter(bounds.getCenter(), map.getBoundsZoomLevel(bounds));    
      map.enableScrollWheelZoom();
    }
  });
};
